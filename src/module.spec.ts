/*
 * Copyright (C) 2020-2022 UBports Foundation <info@ubports.com>
 * Copyright (C) 2020-2022 Johannah Sprinz <hannah@ubports.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as path from "path";
import * as td from "testdouble";
import _test, { TestFn } from "ava";
import { Server, createServer } from "http";
const test = _test as TestFn<{
  server: Server;
}>;
import download, {
  normalizeProgress,
  checkFile,
  ProgressCallback,
  NextCallback
} from "./module.js";
import { rm } from "fs/promises";

const testfile = path.resolve("src/__test-helpers/testfile");
const testtarget = (i: string | number = 1) =>
  path.resolve(`src/__test-helpers/targets/testtarget_${i}`);
const sum = "ef537f25c895bfa782526529a9b63d97aa631564d5d789c2b765448c8635fb6c";

test.before(async t => {
  await rm(path.dirname(testtarget()), { recursive: true }).catch(() => {});
  t.context.server = createServer((req, res) => {
    const sentence = "The quick brown fox jumps over the lazy dog.";
    if (req.url?.endsWith("delay")) {
      res.chunkedEncoding = true;
      (sentence + "\n" + sentence)
        .split(" ")
        .forEach((word, i) => setTimeout(() => res.write(word + " "), i * 100));
      setTimeout(() => {
        res.end();
      }, 2100);
    } else if (req.url?.endsWith("error")) {
      res.statusCode = 404;
      res.end();
    } else {
      res.setHeader("content-length", 44);
      res.write(sentence);
      res.end();
    }
  });
  t.context.server.listen(8080);
});

test.after.always(async t => {
  rm(path.dirname(testtarget()), { recursive: true });
  t.context.server.close();
});

test("normalizeProgress()", async t => {
  t.is(normalizeProgress(1, 100000), 0.00001);
  t.is(normalizeProgress(1, 1000000), 0);
  t.is(normalizeProgress(1, 0), 1);
});

test("checkFile()", async t => {
  t.falsy(
    await checkFile({
      path: testfile,
      checksum: {
        algorithm: "sha256",
        sum
      }
    }),
    "should resolve if file exists and was verified"
  );
  await t.throwsAsync(
    checkFile({
      path: testfile,
      checksum: {
        algorithm: "sha256",
        sum: "mismatch"
      }
    }),
    { message: "checksum mismatch" },
    "should reject on mismatch"
  );
  await t.throwsAsync(
    checkFile({
      path: testfile + "_does_not_exist",
      checksum: {
        algorithm: "sha256",
        sum: "mismatch"
      }
    }),
    { message: /ENOENT/ },
    "should reject on ENOENT"
  );
  await t.throwsAsync(
    checkFile({
      path: testfile
    }),
    { message: "no checksum" },
    "should reject in strict mode without checksum"
  );
  t.falsy(
    await checkFile(
      {
        path: testfile
      },
      false
    ),
    "should resolve in strict mode without checksum"
  );
});

test("download()", async t => {
  const progress = td.func<ProgressCallback>();
  const next = td.func<NextCallback>();
  t.falsy(
    await download(
      [
        {
          url: "http://localhost:8080/testfile",
          path: testtarget(1),
          checksum: {
            algorithm: "sha256",
            sum
          }
        },
        {
          url: "http://localhost:8080/delay",
          path: testtarget(2)
        }
      ],
      progress,
      next
    )
  );
  td.verify(progress(1, 0.0001));
  td.verify(progress(1, 0.000035));
  td.verify(progress(1, 0));
  td.verify(next(1, 2));
  td.verify(next(2, 2));
  await t.throwsAsync(
    download(
      [
        {
          url: "http://localhost:8080/error",
          path: testtarget(2)
        }
      ],
      progress,
      next
    ),
    {
      message:
        "Download Error: http://localhost:8080/error AxiosError: Request failed with status code 404"
    }
  );
  await t.throwsAsync(
    download(
      [
        {
          url: "http://localhost:8080/testfile",
          path: testtarget(1),
          checksum: {
            algorithm: "sha256",
            sum: "mismatch"
          }
        }
      ],
      progress,
      next
    ),
    { message: "checksum mismatch" }
  );
});
