/*
 * Copyright (C) 2020-2022 UBports Foundation <info@ubports.com>
 * Copyright (C) 2020-2022 Johannah Sprinz <hannah@ubports.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { equal } from "assert";
import axios from "axios";
import * as crypto from "crypto";
import * as fs from "fs/promises";
import { dirname } from "path";

export type ProgressCallback = (percentage: number, speed: number) => void;
export type NextCallback = (current: number, total: number) => void;
export type SizeCallback = (size: number) => void;
export interface ChecksumOptions {
  sum?: string;
  algorithm: HashAlgorithmIdentifier;
}

export interface FileOptions extends CheckFileOptions {
  url: string;
}
export interface CheckFileOptions {
  path: string;
  checksum?: ChecksumOptions;
}

export function normalizeProgress(current: number, total: number) {
  return Math.min(
    Math.round((current / Math.max(total, 1)) * 100000) / 100000,
    1
  );
}

/** resolve hex digest string for the checksum of a file */
export async function calculateFileChecksum(
  path: string,
  checksum: ChecksumOptions
): Promise<string> {
  const hash = crypto.createHash(checksum.algorithm as string);
  hash.update(await fs.readFile(path));
  return hash.digest("hex");
}

/** Validate a file based on the checksum. Resolves if the file exists and matches the provided hash, rejects otherwise. */
export async function checkFile(
  { path, checksum }: CheckFileOptions,
  strict: boolean = true
): Promise<void> {
  if (!checksum && strict) throw new Error("no checksum");
  return calculateFileChecksum(path, checksum ?? { algorithm: "md5" }).then(
    sum => checksum && equal(checksum.sum, sum, "checksum mismatch")
  );
}

/** Download a file */
export async function downloadOne(
  url: string,
  downloadPath: string,
  chunkDownloaded: SizeCallback,
  totalSize: SizeCallback
): Promise<void> {
  try {
    await fs.mkdir(dirname(downloadPath), { recursive: true });
    const writer = (await fs.open(downloadPath, "w")).createWriteStream();
    const response = await axios.get(url, {
      responseType: "stream",
      headers: { Accept: "*/*" }
    });
    response.data.pipe(writer);
    totalSize(eval(response.headers["content-length"] || "0"));
    response.data.on("data", ({ length }: { length: number }) =>
      chunkDownloaded(length)
    );
    return new Promise((resolve, reject) => {
      writer.on("finish", resolve);
      writer.on("error", reject);
    });
  } catch (error) {
    throw new Error(`Download Error: ${url} ${error}`);
  }
}

/** download and check files */
export async function download(
  files: FileOptions[],
  progress: ProgressCallback,
  next: NextCallback
): Promise<void> {
  let filesDownloaded = 0;
  const filesToDownload = files.length;
  let overallSize = 0;
  let sizeDownloaded = 1;
  let lastSizeDownloaded = 0;
  const progressInterval = setInterval(() => {
    const downloadProgress = normalizeProgress(sizeDownloaded, overallSize);
    if (overallSize > 0) {
      progress(
        downloadProgress,
        (sizeDownloaded - lastSizeDownloaded) / 1000000
      );
      lastSizeDownloaded = sizeDownloaded;
    }
  }, 1000);
  return Promise.all(
    files.map(({ url, path, checksum }) =>
      checkFile({ path, checksum })
        .catch(() =>
          downloadOne(
            url,
            path,
            chunk => (sizeDownloaded += chunk),
            size => (overallSize += size)
          ).then(() => checkFile({ path, checksum }, false))
        )
        .then(() => next(++filesDownloaded, filesToDownload))
    )
  )
    .then(() => progress(1, 0))
    .finally(() => clearInterval(progressInterval));
}

export default download;
