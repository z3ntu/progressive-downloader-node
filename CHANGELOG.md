# Changelog

This changelog is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

progressive-downloader versioning started at 1.0.0, but this changelog was not added until 2.0.0.

## [Unreleased]

## [2.0.2] - 2022-11-28

### Fixed

- Default export

## [2.0.1] - 2022-11-27

### Changed

- Updated dependencies

### Added

- Bring back CommonJS compatibility

## [2.0.0] - 2022-10-23

### Changed

- Moved to typescript
- Pre-download checksum checks, Downloads, and final checks are now concurrent

### Removed

- activity callback removed from download function
- dropped CommonJS backwards compatibility

### Fixed

- edge-case logic on progress reporting

### Security

- significantly reduced number of dependencies
